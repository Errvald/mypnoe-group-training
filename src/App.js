import React, { useState, useEffect, useRef } from 'react';
import * as signalR from '@aspnet/signalr';
import WelcomeScreen from './WelcomeScreen';
import ActivityTracker from './ActivityTracker';

let _users = [
  {
    name: "Mario Speedwagon",
    lesson: 'Froggy Jumps'
  },
  {
    name: "Petey Cruiser",
    lesson: 'Burpees'
  },
  {
    name: "Anna Sthesia",
    lesson: 'Mountain Climbers'
  },
  {
    name: "Paul Molive",
    lesson: 'Squat Jumps'
  },
  {
    name: "Anna Mull",
    lesson: 'Jumping Jacks to the Step'
  },
  {
    name: "Gail Forcewind",
    lesson: 'Toe Taps with Jumps'
  },
  {
    name: "Paige Turner",
    lesson: 'Side to Side Jumping Lunges'
  },
  {
    name: "Bob Frapples",
    lesson: 'Prisoner Squat Jumps'
  },
  {
    name: "Walter Melon",
    lesson: 'Long Jumps'
  },
  {
    name: "Nick R. Bocker",
    lesson: 'Plyo Jacks'
  }
].map(i => {
  i.stats = {
    fatigue: Math.floor(Math.random() * 198) + 50,
    calories: Math.floor(Math.random() * 198) + 50,
    currentTraining: Math.floor(Math.random() * 200) + 60,
    targetTraining: Math.floor(Math.random() * 60) + 1,
    fat: 50,
    carbs: Math.floor(Math.random() * 100) + 1,
    kcal: Math.floor(Math.random() * 1200) + 115,
    strain: Math.floor(Math.random() * 90) + 10,
    performance: Math.floor(Math.random() * 90) + 10
  }
  i.stats.fat = 100 - i.stats.carbs;
  return i;
});


const App = () => {

  const [isWelcome, setWelcome] = useState(1);

  const [state, setState] = useState({
    gridColumns: 'auto',
    users: _users
  });

  const appRef = useRef();

  const [gridColumns, setGridColumns] = useState('auto');

  useEffect(() => {
    const ReceiveMessage = (id, data) => {
      console.log('ReceiveMessage', id, data);
    }
    const EndSession = (id) => {
      console.log('EndSession', id);
    }
    let connection = null;
    const protocol = new signalR.JsonHubProtocol();
    const transport = signalR.HttpTransportType.WebSockets;

    const options = {
      transport,
      logMessageContent: true,
      logger: signalR.LogLevel.Trace,
      // accessTokenFactory: () => '',
    };

    connection = new signalR.HubConnectionBuilder()
      .withUrl('http://api.mypnoe.com/teamHub', options)
      .withHubProtocol(protocol)
      .build();

    connection.on("ReceiveMessage", ReceiveMessage);
    connection.on("EndSession", EndSession);

    connection.start()
      .then(() => console.info('SignalR Connected'))
      .catch(err => console.error('SignalR Connection Error: ', err));

    return () => {
      connection.stop();
    };
  });

  useEffect(() => {
    const downHandler = ({ key }) => {
      if (key > -1 && key < 10) {
        let users = [..._users];
        setState({ ...state, ...{ users: users.splice(0, key < 1 ? 10 : key) } })
      } else if (key === 'Enter' && isWelcome) {
        setWelcome(0);
      }
    }

    window.addEventListener('keydown', downHandler);

    let users = state.users.length;
    let values = '';
    if (users < 5) {
      values = Array(users).fill('1fr').join(' ');
    } else {
      values = Array(Math.ceil(users / 2)).fill('1fr').join(' ');
    }
    setGridColumns(values);

    return () => {
      window.removeEventListener('keydown', downHandler);
    };

  }, [state, isWelcome]);

  const openFullScreen = () => {
    if (appRef.current.requestFullscreen) {
      appRef.current.requestFullscreen();
    } else if (appRef.current.mozRequestFullScreen) { /* Firefox */
      appRef.current.mozRequestFullScreen();
    } else if (appRef.current.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
      appRef.current.webkitRequestFullscreen();
    } else if (appRef.current.msRequestFullscreen) { /* IE/Edge */
      appRef.current.msRequestFullscreen();
    }
  }

  return (
    <div ref={appRef} className="app">
      {isWelcome ? (
        <WelcomeScreen />
      ) : (
          <div>
            <div className="cards-wrapper">
              <div className="cards"
                style={{ 'gridTemplateColumns': gridColumns }}
              >
                {state.users.map((user, index) => (
                  <ActivityTracker user={user} key={index.toString()} pos={index + 1} count={state.users.length} />
                ))}
              </div>
            </div>
            <div className="sidebar">
              <div className="sidebar__list">
                {state.users.map((user, index) => (
                  <div key={index.toString()} className="list__item"><div>{index + 1}.</div> <div>{user.name}</div></div>
                ))}
              </div>
              <div className="sidebar__footer">
                <div className="sidebar__duration">
                  <span>15'</span><span>12"</span>
                </div>

              </div>
            </div>
          </div>
        )}
      <svg className="fullscreen" onClick={openFullScreen} viewBox="0 0 357 357" width="30px" height="30px">
        <path fill="#6357a0" d="M51,229.5H0V357h127.5v-51H51V229.5z M0,127.5h51V51h76.5V0H0V127.5z M306,306h-76.5v51H357V229.5h-51V306z M229.5,0v51 H306v76.5h51V0H229.5z" />
      </svg>
    </div>
  );
}

export default App;
