import React from 'react';

const InnerCircle = () => {
    return (
        <g transform="rotate(-18 100 100)">
            {/* 0.25 * circum */}
            <circle transform="rotate(-72 100 100)" strokeDasharray="376.99111843077515" strokeDashoffset="301.592894745" r="60" cx="100" cy="100" fill="none" strokeWidth="14" stroke="#029542" />
            <circle transform="rotate(0 100 100)" strokeDasharray="376.99111843077515" strokeDashoffset="301.592894745" r="60" cx="100" cy="100" fill="none" strokeWidth="14" stroke="#1bcc5d" />
            <circle transform="rotate(72 100 100)" strokeDasharray="376.99111843077515" strokeDashoffset="301.592894745" r="60" cx="100" cy="100" fill="none" strokeWidth="14" stroke="#ebbf0b" />
            <circle transform="rotate(144 100 100)" strokeDasharray="376.99111843077515" strokeDashoffset="301.592894745" r="60" cx="100" cy="100" fill="none" strokeWidth="14" stroke="#fa653b" />
            <circle transform="rotate(216 100 100)" strokeDasharray="376.99111843077515" strokeDashoffset="301.592894745" r="60" cx="100" cy="100" fill="none" strokeWidth="14" stroke="#c60204" />
        </g>
    )
}

export default InnerCircle