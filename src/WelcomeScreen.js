import React from 'react'
import InnerCircle from './InnerCircle'

const WelcomeScreen = () => {
    return (
        <svg viewBox="0 0 350 350" width="100%" className="welcome">
            <svg x="75" y="25">
                <mask id="groupMaskFatigue">
                    <circle r="90" fill="none" cx="100" cy="100" strokeWidth="10" stroke="white" />
                </mask>

                <mask id="groupMaskCal">
                    <circle r="76" fill="none" cx="100" cy="100" strokeWidth="10" stroke="white" />
                </mask>

                <svg mask="url(#groupMaskFatigue)">
                    <circle className="progress__circle progress__fade fatigue__fade" r="90" cx="100" cy="100" strokeWidth="10" />
                    <circle className="progress__circle progress__value fatigue__value" r="90" cx="100" cy="100" strokeWidth="10"
                        transform="rotate(-90 100 100)" strokeDasharray="477.522" strokeDashoffset="350" />
                </svg>

                <svg mask="url(#groupMaskCal)">
                    <circle className="progress__circle progress__fade calories__fade" r="76" cx="100" cy="100" strokeWidth="10" />
                    <circle className="progress__circle progress__value calories__value" r="76" cx="100" cy="100"
                        strokeWidth="10" transform="rotate(-90 100 100)" strokeDasharray="477.522" strokeDashoffset="140" />
                </svg>

                <InnerCircle />

                <polygon className="targetTraining" points="92 50,100 35,108 50" fill="#000" transform="rotate(45 100 100)" />
                <circle className="currentTraining" r="5" cx="100" cy="40" fill="#1f1531" transform="rotate(245 100 100)" />
                <g>
                    <text x="100" y="100" fill="white" textAnchor="middle" className="text-bpm-a">135</text>
                    <text x="100" y="100" fill="white" textAnchor="middle" className="text-bpm-b" dy="20">bpm</text>
                </g>
            </svg>

            <svg x="0" y="0">
                <text className="w-text-a" x="-45" y="0" fill="white" style={{"fontSize": "0.8em"}}>Time to begin:</text>
                <text className="w-text-a" x="-45" y="0" dy="25" fill="white" style={{
                    "fontSize": "1.1em",
                    "fontWeight": "bold"
                }}>15:10</text>
            </svg>

            <svg x="80" y="0">
                <text className="w-text-a" x="0" y="16" fill="white" style={{"fontSize": "0.9em"}}>1066 kcal</text>
                <line x1="70" y1="12" x2="200" y2="12" stroke="white" strokeLinecap="round" strokeWidth="1"></line>
                <text className="w-text-a" x="205" y="14" fill="white">Calories Burned</text>
            </svg>

            <svg x="175" y="20">
                <text className="w-text-a" x="160" y="18" fill="#ff8f32">Fatigue</text>
                <line x1="50" y1="15" x2="150" y2="15" stroke="#ff8f32" strokeLinecap="round" strokeWidth="1.5"></line>
                <text className="w-text-a" x="160" y="39" fill="#3759ff">Calories</text>
                <line x1="77" y1="35" x2="150" y2="35" stroke="#3759ff" strokeLinecap="round" strokeWidth="1.5"></line>
            </svg>

            <svg x="0" y="120">
                <text className="w-text-a" x="-20" y="2" fill="white">Beats per minute</text>
                <line x1="50" y1="0" x2="140" y2="0" stroke="white" strokeLinecap="round" strokeWidth="1.5" />
            </svg>

            <svg x="0" y="270">
                <text className="w-text-a" x="87.5" y="-5" fill="#fff" style={{"fontWeight": "300" }}>FAT</text>
                <text className="w-text-a" x="262" y="-5" fill="#fff" textAnchor="end" style={{"fontWeight": "300" }}>CARB</text>
                <mask id="maskv">
                    <rect x="80" width="110" height="14" fill="white"></rect>
                </mask>
                <rect x="80" width="190" height="14" rx="8" fill="#f0d07b"></rect>
                <rect mask="url(#maskv)" x="80" width="190" height="14" rx="8" fill="#c5382e"></rect>
            </svg>

            <svg x="175" y="85">
                <text className="w-text-a" x="160" y="18" fill="#ffffff8a" style={{"fontSize": "0.5em"}}>Training zone</text>
                <text className="w-text-a" x="160" y="18" dy="15" fill="white" style={{"fontSize": "0.6em"}}>Target</text>
                <text className="w-text-a" x="160" y="18" dy="30" fill="white" style={{"fontSize": "0.6em"}}>Current</text>
                <polygon points="133 35,138 26,143 35" fill="white"/>
                <circle r="5" cx="138" cy="45" fill="white"/>
            </svg>
        </svg>
    )
}

export default WelcomeScreen
