import React, { useState, useEffect, useRef } from 'react';
import anime from 'animejs/lib/anime.es.js';
import InnerCircle from './InnerCircle'

const Activitytracker = ({ user, pos, count }) => {

    const [state] = useState({
        size: 100,
        stroke: 10,
        fatigue: {
            r: 90
        },
        calories: {
            r: 76
        }
    });

    const [finished, setFinished] = useState(0);
    const [gratz, setGratz] = useState(0);

    const fatigue = useRef();
    const calories = useRef();
    const fatigueGroup = useRef();
    const caloriesGroup = useRef();
    const targetTraining = useRef();
    const currentTraining = useRef();
    const canvasRef = useRef();
    const jobRef = useRef();
    const cardRef = useRef();

    useEffect(() => {
        const updateCircle = (ref, stat, groupRef) => {
            const data = {
                r: state[stat].r,
                circumference: 2 * Math.PI * state[stat].r
            };
            ref.current.setAttribute('transform', 'rotate(-90 ' + state.size + ' ' + state.size + ')');

            var progress = 1;
            groupRef.current.style.opacity = 1;
            if (user.stats[stat] <= 100) {
                progress = user.stats[stat] / 100;
                groupRef.current.style.opacity = 0;
            }
            ref.current.style.strokeDasharray = data.circumference;

            ref.current.style.strokeDashoffset = data.circumference * (1 - progress);
            console.log(ref.current.style.strokeDasharray, ref.current.style.strokeDashoffset);

            let deg = (user.stats[stat] / 100) * 360;
            groupRef.current.setAttribute('transform', 'rotate(' + deg + ' ' + state.size + ' ' + state.size + ')');
        }

        updateCircle(fatigue, 'fatigue', fatigueGroup);
        updateCircle(calories, 'calories', caloriesGroup);

        anime({
            targets: currentTraining.current,
            transform: 'rotate(' + (((user.stats.currentTraining - 60) + 3) / 146) * 360 + ' 100 100)'
        });

        anime({
            targets: targetTraining.current,
            transform: 'rotate(' + (user.stats.targetTraining / 100) * 360 + ' 100 100)',
            delay: 100
        });

        // currentTraining.current.setAttribute('transform', 'rotate(' + (((user.stats.currentTraining - 60) + 3) / 146) * 360 + ' 100 100)');
        // targetTraining.current.setAttribute('transform', 'rotate(' + (user.stats.targetTraining / 100) * 360 + ' 100 100)');

    }, [user, state]);

    const finishIt = () => {

        if (gratz) {
            
            return;
        }

        setGratz(1);

        if (finished !== 2) {

            anime({
                targets: cardRef.current,
                scale: [
                    { value: 0.9, duration: 100 },
                    { value: 1, duration: 100 }
                ]
            });


            let fillers = [
                { c: '#f29242' },
                { c: '#efaf4b' }
            ];
            let fillersB = [
                { c: '#efaf4b' },
                { c: '#4fd359' },
                { c: '#01abff' }
            ];

            if (finished === 1) {
                fillers = [
                    { c: '#3759ff' },
                    { c: '#01abff' }
                ];
                fillersB = [
                    { c: '#efaf4b' },
                    { c: '#4fd359' },
                    { c: '#01abff' }
                ];
            }


            fillers.map((filler, i) => {
                const circle = createCircle(filler);
                canvasRef.current.appendChild(circle);
                anime({
                    targets: circle,
                    r: 400,
                    delay: i * 700,
                    duration: 1000,
                    easing: "easeOutExpo",
                });
                return filler;
            });
            // Inner circles
            fillersB.map((filler, i) => {
                let cx = anime.random(-50 * 1.15, 200 * 1.15);
                let cy = anime.random(50 * 1.15, 300 * 1.15);
                const circle = createCircle({
                    ...filler,
                    cx,
                    cy
                });
                canvasRef.current.appendChild(circle);
                anime({
                    targets: circle,
                    r: 100,
                    delay: i * 600,
                    duration: 1100,
                    opacity: 0,
                    easing: "easeOutExpo"
                });


                let particles = [];
                for (var pi = 0; pi < 24; pi++) {
                    let particle = createCircle({
                        // c: filler.c,
                        r: anime.random(14, 28),
                        cx,
                        cy
                    });
                    particles.push(particle);
                    canvasRef.current.appendChild(particle);
                }

                anime({
                    targets: particles,
                    r: 0,
                    fill: filler.c,
                    duration: 1200,
                    delay: i * 700,
                    easing: "easeOutExpo",
                    cx: () => {
                        return anime.random(-50 * 1.15, 200 * 1.15);
                    },
                    cy: () => {
                        return anime.random(100 * 1.15, 300 * 1.15);
                    }
                });

                return filler;
            });
            setFinished(finished === 0 ? 1 : 2);
            anime({
                targets: jobRef.current,
                scale: [
                    { value: 0.5 },
                    { value: 1 }
                ],
                opacity: [
                    { value: 0 },
                    { value: 1 }
                ],
                delay: 150
            });
            setTimeout(() => {
                anime({
                    targets: jobRef.current,
                    scale: 1.2,
                    opacity: 0,
                    complete: () => {
                        jobRef.current.style.transform = 'scale(0.5)';
                        jobRef.current.style.opacity = 0;
                    }
                });
                anime({
                    targets: canvasRef.current,
                    opacity: 0,
                    delay: 300,
                    duration: 1200,
                    easing: 'easeInSine',
                    complete: () => {
                        canvasRef.current.innerHTML = '';
                        canvasRef.current.style.opacity = 1;
                        setGratz(0);
                    }
                });
            }, 2500);
        }
    }

    const createCircle = (filler) => {
        let circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
        circle.setAttribute('r', filler.r || 0);
        circle.setAttribute('cx', filler.cx || 100);
        circle.setAttribute('cy', filler.cy || 200);
        circle.setAttribute('fill', filler.c || 'transparent');
        return circle;
    }


    return (
        <div ref={cardRef} className={'card card-count-' + count + (gratz ? ' card--finished' : '') + (finished === 2 ? ' card--performance' : '')} onClick={finishIt}>
            <div className="card__title">
                <h2>{user.name}</h2>
                <h4>{user.lesson}</h4>
                <div className="card__title--calories">
                    <div>{user.stats.kcal}</div>
                    <div>kcal</div>
                </div>
            </div>

            <div className="progress__container">
                <svg className="progress" viewBox="0 0 200 200" width="100%">

                    <radialGradient id="gradShadow">
                        <stop offset="0%" style={{ 'stopColor': '#000', 'stopOpacity': 0.4 }} />
                        <stop offset="100%" style={{ 'stopColor': '#000', 'stopOpacity': 0 }} />
                    </radialGradient>

                    <radialGradient id="gradLight">
                        <stop offset="0%" style={{ 'stopColor': '#fff', 'stopOpacity': 0.4 }} />
                        <stop offset="100%" style={{ 'stopColor': '#fff', 'stopOpacity': 0 }} />
                    </radialGradient>

                    <mask id="groupMaskFatigue">
                        <circle r="90" fill="none" cx="100" cy="100" strokeWidth="10" stroke="white" />
                    </mask>

                    <mask id="groupMaskCal">
                        <circle r="76" fill="none" cx="100" cy="100" strokeWidth="10" stroke="white" />
                    </mask>

                    <svg mask="url(#groupMaskFatigue)">
                        <circle className="progress__circle progress__fade fatigue__fade" r="90" cx="100" cy="100" strokeWidth="10" />
                        <circle ref={fatigue} className="progress__circle progress__value fatigue__value" r="90" cx="100" cy="100" strokeWidth="10" transform="rotate(0 100 100)" />

                        <mask id="thumbMask">
                            <circle r="245" fill="white" cx="257" cy="255" stroke="none" />
                            <rect x="80" y="-80" width="200" height="200" fill="white" />
                        </mask>

                        <g ref={fatigueGroup} className="group fatigue__group" transform="rotate(0 100 100)">

                            <mask id="shadowMaskFatigue">
                                <rect x="100" y="-20" width="100" height="100" fill="white" />
                            </mask>

                            <mask id="lightMaskFatigue">
                                <rect x="0" y="-20" width="100" height="100" fill="white" />
                                <circle className="sphere-mask" r="5" cx="100" cy="10" fill="white" />
                            </mask>

                            <circle className="sphere__shadow" fill="url(#gradShadow)" r="20" cx="100" cy="12" mask="url(#shadowMaskFatigue)" />
                            <circle className="sphere fatigue__sphere" r="5" cx="100" cy="10" mask="url(#shadowMaskFatigue)" />
                            <circle className="sphere__light" fill="url(#gradLight)" r="100" cx="100" cy="12" mask="url(#lightMaskFatigue)" />
                        </g>

                    </svg>

                    <svg mask="url(#groupMaskCal)">
                        <circle className="progress__circle progress__fade calories__fade" r="76" cx="100" cy="100" strokeWidth="10" />
                        <circle ref={calories} className="progress__circle progress__value calories__value" r="76" cx="100" cy="100" strokeWidth="10" transform="rotate(0 100 100)" />

                        <g ref={caloriesGroup} className="group calories__group" transform="rotate(0 100 100)">

                            <mask id="shadowMaskCal">
                                <rect x="100" y="-20" width="100" height="100" fill="white" />
                            </mask>

                            <mask id="lightMaskCal">
                                <rect x="0" y="-20" width="100" height="100" fill="white" />
                                <circle className="sphere-mask" r="5" cx="100" cy="24" fill="white" />
                            </mask>

                            <circle className="sphere__shadow" fill="url(#gradShadow)" r="20" cx="100" cy="24" mask="url(#shadowMaskCal)" />
                            <circle className="sphere calories__sphere" r="5" cx="100" cy="24" mask="url(#shadowMaskCal)" />
                            <circle className="sphere__light" fill="url(#gradLight)" r="76" cx="100" cy="24" mask="url(#lightMaskCal)" />
                        </g>
                    </svg>

                    {/* Inner circle */}
                    <InnerCircle />
                    <polygon ref={targetTraining} className="targetTraining" points="92 50,100 35,108 50" fill="#1f1531" transform="rotate(0 100 100)" />
                    <circle ref={currentTraining} className="currentTraining" r="5" cx="100" cy="40" fill="#1f1531" transform="rotate(0 100 100)" />
                    <g>
                        <text x="100" y="100" fill="white" textAnchor="middle" className="text-bpm-a">{user.stats.currentTraining}</text>
                        <text x="100" y="100" fill="white" textAnchor="middle" className="text-bpm-b" dy="20">bpm</text>
                    </g>

                </svg>
            </div>
            <div className="fat-carb">
                <div className="fat-carb__head">
                    <div className="fat-carb__fat">FAT</div>
                    <div className="fat-carb__carbs">CARB</div>
                </div>
                <div className="fat-carb__progress"><div className="fat-carb__slider" style={{ 'width': user.stats.fat + '%' }}></div></div>
                <div className="fat-carb__footer">
                    <div>{user.stats.fat}%<div>Fat</div></div>
                    <div>{user.stats.carbs}%<div>Carbs</div></div>
                </div>
            </div>
            <div className="performance">
                <div>Performance</div>
                <div>{user.stats.performance}%</div>
            </div>
            <div className="strain">
                <div className="strain__head">
                    <div className="strain__val">80%</div>
                    <div className="strain__label">Strain</div>
                </div>
                <div className="strain__progress"><div className="strain__slider" style={{ 'width': user.stats.strain + '%' }}></div></div>
            </div>
            <svg ref={canvasRef} viewBox="0 0 200 400" width="100%" height="100%" className="c"></svg>
            <div ref={jobRef} className="great-job">
                <div className="great-job__title">Great <div>{user.name}! <div>{finished === 1 ? "Fatigue" : "Calories"} target reached!</div></div></div>
                <svg className="great-job__thumbsup" viewBox="-3 0 511 511.99986">
                    <path mask="url(#thumbMask)" className="thumb__a" d="m463.085938 280.710938c0 127.738281-103.5625 231.289062-231.296876 231.289062-31.335937 0-61.207031-6.230469-88.453124-17.523438-74.46875-30.847656-129.320313-99.476562-140.660157-181.976562-1.441406-10.390625-2.175781-21.003906-2.175781-31.789062 0-127.746094 103.550781-231.296876 231.289062-231.296876 127.734376 0 231.296876 103.550782 231.296876 231.296876zm0 0" fill="#322941" />
                    <path mask="url(#thumbMask)" className="thumb__b" d="m231.789062 49.414062c-12.230468 0-24.234374.957032-35.949218 2.785157 110.667968 17.269531 195.351562 112.992187 195.351562 228.511719 0 115.503906-84.675781 211.222656-195.335937 228.5 11.710937 1.828124 23.707031 2.789062 35.929687 2.789062 127.738282 0 231.300782-103.550781 231.300782-231.289062 0-127.746094-103.5625-231.296876-231.296876-231.296876zm0 0" fill="#6c62a0" />

                    <g className="thumb__thumb" mask="url(#thumbMaskb)">
                        <path className="thumb__c" d="m491.960938 263.613281-132.074219 87.457031c-9.410157 6.234376-20.808594 8.707032-31.949219 6.925782-11.167969-1.777344-22.59375.660156-32.027344 6.90625l-17.675781 11.714844-98.984375-145.910157 24.285156-104.914062c1.886719-8.167969 2.820313-16.445313 2.820313-24.691407 0-21.332031-6.238281-42.386718-18.203125-60.460937-2.941406-4.445313-4.347656-9.472656-4.347656-14.4375 0-8.484375 4.117187-16.808594 11.726562-21.847656 10.253906-6.792969 23.84375-5.53125 32.671875 3.03125l15.734375 15.238281c17.992188 17.457031 31.871094 38.710938 40.585938 62.21875l8.53125 22.96875 87.855468-58.046875c14.03125-9.289063 33.011719-5.226563 41.972656 9.144531 8.640626 13.84375 4.203126 32.136719-9.410156 41.148438-.164062.109375-.175781.285156-.121094.417968.078126.144532.242188.242188.425782.164063 3.472656-1.335937 7.105468-1.984375 10.707031-1.984375 5.75 0 11.449219 1.65625 16.324219 4.808594 3.359375 2.171875 6.335937 5.058594 8.683594 8.605468 4.535156 6.851563 5.886718 14.886719 4.402343 22.355469-2.328125 11.765625-10.878906 17.828125-12.558593 18.96875-.285157.199219-.363282.59375-.164063.890625 0 .007813.011719.019532.019531.019532.1875.265624.570313.339843.84375.167968 2.394532-.582031 4.832032-.859375 7.269532-.859375 5.949218 0 11.867187 1.691407 16.898437 4.898438 3.402344 2.175781 6.410156 5.050781 8.738281 8.574219 9.132813 13.789062 5.359375 32.375-8.445312 41.511718-.339844.230469-.679688.460938-1.019532.679688-.195312.15625-.253906.40625-.152343.582031.019531.042969.066406.089844.109375.109375.152344.121094.394531.132812.59375-.019531 8.574218-2.921875 17.90625-1.59375 25.414062 3.269531 3.457032 2.21875 6.523438 5.207031 8.949219 8.871094 9.136719 13.800781 5.359375 32.386718-8.429687 41.523437zm0 0" fill="#fdd7bd" />
                        <path className="thumb__d" d="m279.496094 404.308594-55.71875 36.898437-80.441406 53.269531c-74.46875-30.851562-129.320313-99.46875-140.660157-181.976562l93.351563-61.820312 56.828125-37.636719c6.542969-4.335938 15.371093-2.535157 19.707031 4.019531l110.953125 167.527344c4.335937 6.554687 2.535156 15.367187-4.019531 19.71875zm0 0" fill="#4663f2" />
                        <path className="thumb__e" d="m279.496094 404.308594-55.71875 36.898437-127.75-190.527343 56.828125-37.636719c6.542969-4.335938 15.371093-2.535157 19.707031 4.019531l110.953125 167.523438c4.335937 6.558593 2.535156 15.371093-4.019531 19.722656zm0 0" fill="#18aaf2" />
                        <g fill="#fac5aa">
                            <path d="m440.808594 103.460938-43.949219 23.519531c-6.234375 3.328125-13.964844 1.589843-18.15625-4.085938l34.648437-22.417969c.078126.144532.242188.242188.425782.164063 3.472656-1.335937 7.105468-1.984375 10.707031-1.984375 5.753906 0 11.449219 1.65625 16.324219 4.804688zm0 0" />
                            <path d="m466.203125 158.511719-42.992187 22.988281c-6.234376 3.339844-13.964844 1.605469-18.160157-4.070312l34.648438-22.421876 1.496093-.703124c.1875.265624.570313.339843.84375.167968 2.394532-.582031 4.832032-.859375 7.269532-.859375 5.949218 0 11.867187 1.691407 16.894531 4.898438zm0 0" />
                            <path d="m491.445312 213.21875-37.867187 20.257812c-6.226563 3.324219-13.953125 1.589844-18.160156-4.085937l30.015625-19.421875c.15625.121094.398437.132812.59375-.023438 8.574218-2.917968 17.90625-1.589843 25.417968 3.273438zm0 0" />
                        </g>
                    </g>

                </svg>
            </div>
        </div>
    );
}

export default Activitytracker;
